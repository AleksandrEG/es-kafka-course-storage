package com.epam.ekc.storage.service;

import com.epam.ekc.storage.model.Author;
import com.epam.ekc.storage.model.Book;
import com.epam.ekc.storage.model.Identifiable;
import com.epam.ekc.storage.repository.BookRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageConsumer {
    
    private final ObjectMapper   objectMapper;
    private final BookRepository bookRepository;
    
    @KafkaListener(id = "single-listener",
                   topics = "storage.entity",
                   groupId = "search.one-by-one-consumer",
                   containerFactory = "singleConsumer")
    public void handleSingleMessage(ConsumerRecord<String, String> message) {
        Class<? extends Identifiable> type = resolveType(message.key());
        if (type == null) {
            return;
        }
        if (type == Book.class) {
            bookRepository.save(convertToBook(message.value()));
        }
    }
    
    @KafkaListener(id = "single-listener",
                   topics = "storage.entity",
                   groupId = "search.one-by-one-consumer",
                   containerFactory = "singleConsumer")
    public void handleBatchMessage(List<ConsumerRecord<String, String>> messageList) {
        var books = messageList
                            .stream()
                            .map(this::retrieveBook)
                            .collect(Collectors.toList());
        bookRepository.saveAll(books);
    }
    
    private Book retrieveBook(ConsumerRecord<String, String> message) {
        Class<? extends Identifiable> type = resolveType(message.key());
        if (type == null) {
            return null;
        }
        if (type == Book.class) {
            return convertToBook(message.value());
        } else {
            return null;
        }
    }
    
    @SneakyThrows
    private Book convertToBook(String json) {
        return objectMapper.readValue(json, Book.class);
    }
    
    private Class<? extends Identifiable> resolveType(String key) {
        var type = key.split("\\|")[0];
        switch (type) {
            case "Author": {
                return Author.class;
            }
            case "Book": {
                return Book.class;
            }
            default: {
                return null;
            }
        }
    }
}

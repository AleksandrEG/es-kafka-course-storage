package com.epam.ekc.storage.service;

import com.epam.ekc.storage.model.Identifiable;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageProducer {
    
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;
    
    private final String STORAGE_TOPIC = "storage.entity";
    
    @SneakyThrows
    public void sendMessage(Identifiable identifiable) {
        log.debug("Sending to kafka topic {}, object: {}", identifiable, STORAGE_TOPIC);
        var jsonValue = objectMapper.writeValueAsString(identifiable);
        var messageKey = identifiable.getClass().getSimpleName().concat("|").concat(identifiable.getId());
        kafkaTemplate.send(STORAGE_TOPIC, messageKey, jsonValue);
    }
}
